#![allow(non_camel_case_types)]

#![feature(libc)]
extern crate libc;

use libc::{c_int, c_void};


pub type EGLNativeDisplayType = *mut c_void;
pub type EGLNativeWindowType = *mut c_void;
pub type EGLNativePixmapType = *mut c_void;


pub type khronos_uint64_t = u64;
pub type khronos_utime_nanoseconds_t = khronos_uint64_t;
pub type khronos_ssize_t = isize;
pub type NativeDisplayType = EGLNativeDisplayType;
pub type NativePixmapType = EGLNativePixmapType;
pub type NativeWindowType = EGLNativeWindowType;
pub type EGLint = c_int;

#[link(name = "EGL")]
extern "C" {}
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
